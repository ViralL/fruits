
$(document).ready(function() {
    var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (screen_width >= 768) {
        $('#fruits').parallax();
    }

    $('.navbar-toggle').click(function() {
        $('nav').toggleClass('active1');
        $('body').toggleClass('act');
    });
    $(document).on('click',function(){
        $('.collapse').collapse('hide');
        $('nav').toggleClass('active1');
    });
    function count_totals(){

        var total = 0;  // значение общей суммы заказа
        $('.cart_item').each(function(){

            var price = $(this).attr('data-price'); // исходная цена товар
            var val = $(this).find(".product-quantity__input").attr('data-val');
            var qty = $(this).find(".product-quantity__input").val(); // значение value number
            //console.log(val);
            var summ = price*qty; // значение конечной цены за товар
            total+=summ;


        });
        $(".order-amount__second h1").text(total + ' P'); // обновляем общую сумму
        $("#order-id").find('.num').text(total + ' рублей');
        $("#order-id").find('.num').val(total);
    }
    //count_totals();
    $("input").on("change",function(){
        count_totals();
    });


    ////button input
    $('.inc').click(function(){
        var $t=$(this).parent().parent().find('input');
        var step = $(this).parent().parent().find("input").attr('data-step');
        var count = parseInt($t.val()) - parseFloat(step);
        count = count < 1 ? 1 : count;
        $t.val(count);
        //var vI = Math.abs(parseInt($t.val()));
        //$t.val(vI?--vI:0);
        //console.log(count_totals());
        $('.order-amount__second h1').html(count_totals());
        return false;

    });
    $('.dec').click(function(){
        var $t=$(this).parent().parent().find('input');
        var step = $(this).parent().parent().find("input").attr('data-step');
        $($t).css('display','inline-block');
        //var vI = Math.abs(parseFloat($t.val()));
        //var half = 0.5;
        //$t.val(vI?++vI:0.5);
        $t.val(parseFloat($t.val()) + parseFloat(step));
        $t.change();
        //console.log(count_totals());
        var val = $(this).parent().parent().find("input").attr('data-val');
        $(this).parent().parent().find(".product-quantity__val").text(val);
        $('.order-amount__second h1').html(count_totals());
        return false;
    });


    // count input
    //$('.input__quantity-js .inc').click(function () {
    //    var $input = $(this).parents('.input__quantity-js').find('input');
    //    var count = parseInt($input.val()) - 1;
    //    count = count < 0 ? 0 : count;
    //    $input.val(count);
    //    $input.change();
    //    return false;
    //});
    //$('.input__quantity-js .dec').click(function () {
    //    var $input = $(this).parents('.input__quantity-js').find('input');
    //    var count = parseInt($input.val()) + 1;
    //    count = count > 999 ? 1 : count;
    //    $input.val(count);
    //    $input.change();
    //    return false;
    //});

    //$('.calculator__body').find('.footer-price').append('<h1><span class="cur">0</span> <span class="rub">Р</span></h1>');
    //function valParam () {
    //    var Price = $('.box-filter').attr('data-price'); //получаем стоимость
    //    var roofValue = $('#fruit-1').val();//вычисляем количество
    //    var roofValue1 = $('#fruit-2').val();//вычисляем количество
    //        if (!$.isNumeric(roofValue)) roofValue = 1;
    //        if (!$.isNumeric(roofValue)) roofValue1 = 1;
    //        var total = Price * roofValue * roofValue1;//формула расчета общей стоимости
    //        var newTotal = Math.round(total);//округляем
    //        $('.cur').html(newTotal);
    //        $('.rub').show();
    //        console.log(total);
    //
    //};
    ////отслеживаем изменение данных
    //$('.calculator').on('click keyup', valParam);
    //

    $(window).scroll(function () {
        if ($(document).scrollTop() > 500) {
            $('.navbar').addClass('active');
        } else {
            $('.navbar').removeClass('active');
        }
    });


    // phone
    $("[name=phone]").mask("+7 (999) 999-9999");

     // anchor
     $('.anchor a').bind("click", function(e){
         var anchor = $(this);
         $('html, body').stop().animate({
             scrollTop: $(anchor.attr('href')).offset().top
         }, 1000);
         e.preventDefault();
     });
    return false;

});


