<?php
    // для вывода ошибок

	header("Content-type: text/html; charset=UTF-8");
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);

	date_default_timezone_set('Europe/Moscow');
	require_once "lib/PHPMailerAutoload.php";

	$mail = new PHPMailer;
	//$mail -> SMTPDebug = 3; // для вывода ошибок
	$mail -> IsSMTP();
	$mail -> Host = "smtp.yandex.ru";
	$mail -> SMTPAuth = true;
	$mail -> SMTPSecure = "tls";
	$mail -> Port = 587;
	$mail -> CharSet = "UTF-8";

	$body =
	"<table>
		<tr>
			<td colspan=2>
				<h2>Вам пришла заявка</h2>
			</td>
		</tr>
		<tr>
			<td width=100>
				Имя:
			</td>
			<td>
				".$_POST['name']."
			</td>
		</tr>
		<tr>
			<td width=100>
				E-mail:
			</td>
			<td>
				".$_POST['email']."
			</td>
		</tr>
        <tr>
			<td width=100>
				Телефон:
			</td>
			<td>
				".$_POST['phone']."
			</td>
		</tr>
        <tr>
			<td width=100>
				Сообщение:
			</td>
			<td>
				".$_POST['message']."
			</td>
		</tr>
        <tr>
			<td width=100>
				Сумма:
			</td>
			<td>
				".$_POST['count']."
			</td>
		</tr>
	</table>";

	$mail -> Username = "test@verstkalab.ru";  // <-- Авторизация (почта на яндекс)
	$mail -> Password = "ver1000000";        // <-- Авторизация (пароль от почты)
	$mail -> SetFrom("test@verstkalab.ru", "Уведомитель о заявках"); // <-- От кого (должно совпадать с почтой яндекс)
	$mail -> Subject = "Заявка с сайта";
	$mail -> MsgHTML($body);

	$mail -> AddAddress("ann.kiss@mail.ru", "Менеджеру продаж"); // <-- Кому отправляем

	if($mail -> Send())
		echo "Письмо отправлено.";
	else
		echo "Что-то пошло не так.";

?>

