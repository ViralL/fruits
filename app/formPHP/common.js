// Валидатор форм
var formValidator = {
    initialize: function() {
        this.setUpListeners();
    },
    setUpListeners: function() {
        $('form').on('submit', formValidator.submitForm);
        $('form').on('keyup', 'input', formValidator.removeError);
    },
    submitForm: function(e) {
        e.preventDefault();

        var form = $(this),
            url = form.attr("action"),
            submitBtn = form.find('button[type="submit"]');

        if (formValidator.validate(form) === false) return false;

        // Отправка формы
        var data = form.serialize(),
            goal = $.unserialize(data).goal,
            modal = $('#' + form.parents('.modal').attr('id')),
            success_modal = $(submitBtn.data('success-modal'));

        $.ajax({
            url: url,
            type: "POST",
            data: data,
            beforeSend: function() {
                submitBtn.attr('disabled', 'disabled');
            },
            success: function(data) {
                if (modal.selector != '#undefined') {
                    modal.one('hidden.bs.modal', function() {
                        success_modal.modal('show');
                    });
                    modal.modal('hide');
                } else success_modal.modal('show');
            },
            complete: function() {
                submitBtn.removeAttr('disabled');
            },
            error: function(data) {
                alert(data.responseText);
            }
        });
    },
    validate: function(form) {
        var inputs = form.find('input[required],textarea[required]'),
            valid = true;

        inputs.tooltip('destroy');

        $.each(inputs, function(index, val) {
            var input = $(val),
                val = input.val(),
                formGroup = input.parents('.form-group'),
                label = input.data('label') == undefined ? formGroup.find('label').text() : input.data('label'), //.toLowerCase(),
                textError = label;

            if (val.length === 0) {
                formGroup.addClass('has-error').removeClass('has-success');
                input.tooltip({
                    trigger: 'manual',
                    placement: 'top',
                    title: textError
                }).tooltip('show');

                valid = false;
            } else {
                formGroup.addClass('has-success').removeClass('has-error');
            }
        });

        return valid;
    },

    removeError: function() {
        $(this).tooltip('destroy').parents('.form-group').removeClass('has-error');
    },

    clearForm: function(form) {
        form
            .find(':input')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    }
}

$(function() {
    
    // инициализируем валидацию форм
    formValidator.initialize();
    
});